import { useState, useContext, useEffect } from 'react';
import Card from './shared/Card';
import RatingSelect from './RatingSelect';
import Button from './shared/Button';
import FeedbackContext from '../context/FeedbackContext';

function FeedbackForm({ handleAddReview }) {
  const { addFeedback, updateFeedback, submitEditedFeedback } =
    useContext(FeedbackContext);

  useEffect(() => {
    if (updateFeedback.edit == true) {
      setBtnDisabled(false);
      setReviewText(updateFeedback.item.text);
      setRating(updateFeedback.item.rating);
      console.log('useEffect ran');
    }
  }, [updateFeedback]);

  const [reviewText, setReviewText] = useState('');
  const [rating, setRating] = useState(10);
  const [btnDisabled, setBtnDisabled] = useState(true);
  const [message, setMessage] = useState('');

  const handleTextChange = (e) => {
    setReviewText(e.target.value);
    if (reviewText === '') {
      setBtnDisabled(true);
      setMessage(null);
    } else if (reviewText !== '' && reviewText.trim().length < 10) {
      setBtnDisabled(true);
      setMessage('Minimum length is 10 characters');
    } else {
      setBtnDisabled(false);
      setMessage(null);
    }
  };

  const handleRatingSelected = (rating) => {
    setRating(rating);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (reviewText.trim().length >= 10) {
      const newFeedback = {
        text: reviewText,
        rating: rating,
      };

      if (updateFeedback.edit === true) {
        submitEditedFeedback(updateFeedback.item.id, newFeedback);
      } else {
        addFeedback(newFeedback);
      }
      setReviewText('');
    }
  };

  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <h2>How is our service?</h2>
        <RatingSelect select={handleRatingSelected} />
        <div className='input-group'>
          <input
            type='text'
            onChange={handleTextChange}
            placeholder='Write a review'
            value={reviewText}
          />
          <Button type='submit' isDisabled={btnDisabled}>
            Send
          </Button>
        </div>

        {message && <div className='message'>{message}</div>}
      </form>
    </Card>
  );
}

export default FeedbackForm;
