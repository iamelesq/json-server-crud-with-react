const FeedbackData = [
  {
    id: '1',
    rating: 10,
    text: 'Lickin boabium fur dollor, sit amet consectetur adipisicing elit. Ipsam, modi aperiam ipsa officia incidunt quo doloribus molestias autem.',
  },
  {
    id: '2',
    rating: 3,
    text: 'Loren rimmin fur dolor, sit lap dippid sawsage elit. Perferendis all ex fani?',
  },
  {
    id: '3',
    rating: 7,
    text: 'Horen album, sat mamet consec tittur adipis clit. Molestias autem.',
  },
  {
    id: '4',
    rating: 6,
    text: 'Lorem dipin, shit aumet consecteturo adipisicingly elitato. Molestias autem.',
  },
];

export default FeedbackData;
